﻿using Caliburn.Micro;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace wpf.ViewModels.Dialogs
{
    class RemoveDialogViewModel : Screen
    {
        public void Yes()
        {
            TryClose(true);
        }

        public void No()
        {
            TryClose(false);
        }
    }
}
