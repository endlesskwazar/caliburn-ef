﻿using Caliburn.Micro;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace wpf.ViewModels
{
    public class MainShellViewModel : Conductor<object>
    {
        public void GoToClientShell()
        {
            ActivateItem(new wpf.ViewModels.Clients.ClientShellViewModel());
        }

        public void GoToAccountShell()
        {
            ActivateItem(new wpf.ViewModels.Accounts.AccountShellViewModel());
        }

        public void GoToAccountTypeShell()
        {
            ActivateItem(new wpf.ViewModels.Types.AccountTypeShellViewModel());
        }

        public void GoToStatisticShell()
        {
            ActivateItem(new wpf.ViewModels.Statistics.StatistecShellViewModel());
        }
    }
}
