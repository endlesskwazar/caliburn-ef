﻿using Caliburn.Micro;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using wpf.Models;

namespace wpf.ViewModels.Clients
{
    public class UpdateClientViewModel : Screen
    {
        private readonly BankDataContext context = new BankDataContext();

        private CLIENT client;

        public CLIENT Client
        {
            get { return client; }
            set { client = value; }
        }

        public void Update()
        {
            context.SaveChanges();
            TryClose(true);
        }

        public UpdateClientViewModel(int id)
        {
            client = context.CLIENTs.Find(id);
        }
    }
}
