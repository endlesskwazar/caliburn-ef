﻿using Caliburn.Micro;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using wpf.Models;

namespace wpf.ViewModels.Clients
{
    class AddClientViewModel : Screen
    {
        private BankDataContext context = new BankDataContext();

        private CLIENT client = new CLIENT();

        public  CLIENT Client
        {
            get { return client; }
            set
            {
                client = value;
                NotifyOfPropertyChange(() => Client);
            }
        }



        public void AddClient()
        {

        }

    }
}
