﻿using Caliburn.Micro;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace wpf.ViewModels.Clients
{
    public class ClientShellViewModel : Conductor<object>
    {
        public ClientShellViewModel()
        {
            ActivateItem(new ClientListViewModel());
        }

        public void GoToAddClient()
        {
            ActivateItem(new AddClientViewModel());
        }
    }
}
