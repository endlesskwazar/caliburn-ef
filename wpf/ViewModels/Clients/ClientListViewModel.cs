﻿using Caliburn.Micro;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using wpf.Models;

namespace wpf.ViewModels.Clients
{
    public class ClientListViewModel : Screen
    {
        private BankDataContext context = new BankDataContext();

        private CLIENT selectedClient;

        public CLIENT SelectedClient
        {
            get { return selectedClient; }
            set
            {
                selectedClient = value;
                NotifyOfPropertyChange(() => SelectedClient);
            }
        }


        private BindableCollection<CLIENT> clients;

        public BindableCollection<CLIENT> Clients
        {
            get { return clients; }
            set
            {
                clients = value;
                NotifyOfPropertyChange(() => Clients);
            }
        }

        public ClientListViewModel()
        {
            clients = new BindableCollection<CLIENT>(context.CLIENTs.ToList());
        }

        public void AddClient()
        {
            if (selectedClient == null)
                return;
            IWindowManager wm = new WindowManager();
            UpdateClientViewModel updateClientViewModel = new UpdateClientViewModel(selectedClient.ID);

            if((bool)wm.ShowDialog(updateClientViewModel, null, null))
            {
                context = new BankDataContext(); //i`m doing it, sorry
                Clients = new BindableCollection<CLIENT>(context.CLIENTs.ToList());
            }
        }
    }
}
