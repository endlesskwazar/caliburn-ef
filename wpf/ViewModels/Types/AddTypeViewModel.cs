﻿using Caliburn.Micro;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using wpf.Models;

namespace wpf.ViewModels.Types
{
    class AddTypeViewModel : Screen
    {
        private readonly BankDataContext context = new BankDataContext();

        private ACCOUNTTYPE type = new ACCOUNTTYPE();

        public ACCOUNTTYPE Type
        {
            get { return type; }
            set { type = value; }
        }

        public AddTypeViewModel()
        {
            type = new ACCOUNTTYPE();
        }

        public void AddType()
        {
            context.ACCOUNTTYPEs.Add(type);
            context.SaveChanges();
            type = new ACCOUNTTYPE();
        }
    }
}
