﻿using Caliburn.Micro;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace wpf.ViewModels.Types
{
    class AccountTypeShellViewModel : Conductor<object>
    {
        public AccountTypeShellViewModel()
        {
            ActivateItem(new TypesViewModel());
        }

        public void GoToAddType()
        {
            ActivateItem(new AddTypeViewModel());
        }
    }
}
