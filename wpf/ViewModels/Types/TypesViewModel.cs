﻿using Caliburn.Micro;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using wpf.Models;

namespace wpf.ViewModels.Types
{
    public class TypesViewModel : Screen
    {
        private readonly BankDataContext context = new BankDataContext();

        private BindableCollection<ACCOUNTTYPE> types;

        public BindableCollection<ACCOUNTTYPE> Types
        {
            get { return types; }
            set
            {
                types = value;
                NotifyOfPropertyChange(() => Types);
            }
        }

        private String searchText;

        public String SearchText
        {
            get { return searchText; }
            set { searchText = value; }
        }

        public void SaveChanges()
        {
            context.SaveChanges();
        }

        public void Search()
        {
            var searchQuery = from searchItem in context.ACCOUNTTYPEs
                              where searchItem.TITLE.Contains(searchText)
                              select searchItem;
            Types = new BindableCollection<ACCOUNTTYPE>(searchQuery.ToList());
        }

        public TypesViewModel()
        {
            types = new BindableCollection<ACCOUNTTYPE>(context.ACCOUNTTYPEs.ToList());
        }

    }
}
