﻿using Caliburn.Micro;
using System;
using LiveCharts;
using LiveCharts.Wpf;
using wpf.Models;
using System.Collections.Generic;
using System.Linq;

namespace wpf.ViewModels.Statistics
{
    class AccountStatisticViewModel : Screen
    {
        private readonly BankDataContext context = new BankDataContext(); 

        public AccountStatisticViewModel()
        {
            IList<ACCOUNTTYPE> types = new List<ACCOUNTTYPE>(context.ACCOUNTTYPEs.ToList());

            SeriesCollection = new SeriesCollection();

            foreach(var type in types)
            {
                ColumnSeries col = new ColumnSeries();
                col.Title = type.TITLE;
                col.Values = new ChartValues<int> { type.ACCOUNTs.Count };
                SeriesCollection.Add(col);
            }
      

            Labels = new string[context.ACCOUNTTYPEs.Count()];

            int i = 0;
            foreach(var type in types)
            {
                Labels[i] = type.TITLE;
            }

            Formatter = value => value.ToString("N");
        }

        public SeriesCollection SeriesCollection { get; set; }
        public string[] Labels { get; set; }
        public Func<double, string> Formatter { get; set; }
    }
}
