﻿using Caliburn.Micro;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace wpf.ViewModels.Statistics
{
    class StatistecShellViewModel : Conductor<object>
    {
        private string selectedPage = null;
        public string SelectedPage
        {
            get { return selectedPage; }
            set
            {
                selectedPage = value;
                Navigate();
            }
        }

        public string[] Pages
        {
            get { return new string[] { "Accounts", "Balance", "Client" }; }
            set { }
        }
            
        public void Navigate()
        {
            switch(SelectedPage)
            {
                case "Accounts":
                    ActivateItem(new wpf.ViewModels.Statistics.AccountStatisticViewModel());
                    break;
                case "Balance":
                    break;
                case "Client":
                    break;
                default:
                    break;
            }
        }
    }
}
