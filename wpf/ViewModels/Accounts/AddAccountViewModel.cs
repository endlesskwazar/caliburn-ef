﻿using Caliburn.Micro;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using wpf.Models;


namespace wpf.ViewModels.Accounts
{
    public class AddAccountViewModel : Screen
    {
        private readonly BankDataContext context = new BankDataContext();

        private ACCOUNT account;

        public ACCOUNT Account
        {
            get { return account; }
            set
            {
                account = value;
            }
        }

        private BindableCollection<CLIENT> clients;

        public BindableCollection<CLIENT> Clients
        {
            get { return clients; }
            set { clients = value; }
        }

        private BindableCollection<ACCOUNTTYPE> types;

        public BindableCollection<ACCOUNTTYPE> Types
        {
            get { return types; }
            set { types = value; }
        }

        public void AddAccount()
        {
            if (!Account.IsValid)
                return;

            context.ACCOUNTs.Add(account);
            context.SaveChanges();
        }

        public AddAccountViewModel()
        {
            clients = new BindableCollection<CLIENT>(context.CLIENTs.ToList());
            types = new BindableCollection<ACCOUNTTYPE>(context.ACCOUNTTYPEs.ToList());
            account = new ACCOUNT();
        }


    }
}
