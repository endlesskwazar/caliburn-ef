﻿using Caliburn.Micro;
using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using wpf.Models;
using wpf.ViewModels.Dialogs;

namespace wpf.ViewModels.Accounts
{
    class AccountViewModel : Screen
    {
        private readonly BankDataContext context = new BankDataContext();

        private BindableCollection<ACCOUNT> accounts;

        public BindableCollection<ACCOUNT> Accounts
        {
            get { return accounts; }
            set
            {
                accounts = value;
                NotifyOfPropertyChange(() => Accounts);
            }
        }

        private string searchText;

        public string SearchText
        {
            get { return searchText; }
            set { searchText = value; }
        }

        public void Search()
        {
            var res = from r in context.ACCOUNTs
                      where r.CLIENT.SNAME.Contains(searchText)
                      select r;
            Accounts = new BindableCollection<ACCOUNT>(res.ToList());
        }

        private ACCOUNT selectedAccount;

        public ACCOUNT SelectedAccount
        {
            get { return selectedAccount; }
            set
            {
                selectedAccount = value;
                NotifyOfPropertyChange(() => SelectedAccount);
            }
        }


        public void DeleteAccount()
        {
            if (selectedAccount == null)
                return;

            //crete instance of wm and vm
            IWindowManager windowManager = new WindowManager();
            RemoveDialogViewModel removeDialog = new RemoveDialogViewModel();

            //create settings object
            dynamic settings = new ExpandoObject();
            settings.Title = "Remove Account";

            //show dialog and if yes remove
            if (windowManager.ShowDialog(removeDialog, null, settings))
            {
                context.ACCOUNTs.Remove(selectedAccount);
                context.SaveChanges();
                Accounts.Remove(selectedAccount);
            }
           
        }

        public AccountViewModel()
        {
            accounts = new BindableCollection<ACCOUNT>(context.ACCOUNTs.ToList());
            var k = 1;
        }

    }
}
