﻿using Caliburn.Micro;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace wpf.ViewModels.Accounts
{
    public class AccountShellViewModel : Conductor<object>
    {
        public AccountShellViewModel()
        {
            ActivateItem( new AccountViewModel());
            
        }

        public void GoToAddAccount()
        {
            ActivateItem(new AddAccountViewModel());
        }
    }
}
